function out = getLGNMOVParameters()

out.maindatafolder = '/home/joao/lgnmov';
out.klustersfolder = 'klusters_data';
out.scGUIfolder = 'scGUI_data';
out.rawfolder = 'raw_data';
out.eventsfolder = 'events_data';
out.processedfolder = 'processed_data';
out.logsfolder = 'stimlog_data';
out.experiment_folder_names = {'tf','sf','ori','gratings'};
out.experiment_types = {'tf','sf','ori','ori'};