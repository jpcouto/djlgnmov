%% Plot schema
erd(lgnmov_experiment.getSchema())
%% get behavior for all experiments
behav = fetch(lgnmov_experiment.Behavior,'behavior_time','behavior_velocity');

fig = figure();
hold all

for ii = 1:length(behav)
    plot(behav(ii).behavior_time,behav(ii).behavior_velocity + ii*50)
end
%% Histograms of modulation index...
sel = lgnmov_experiment.LocomotionModulationUnits()*lgnmov_experiment.RecordingSession()*lgnmov_experiment.VisualEvokedIndexUnits() ...
    & 'recording_site = "lgn"' & ... & select brain area
    'experiment_type = "tf"' & ... % select experiment
    'vevoked > 10';  % Select threshold
f1mod = fetch(sel,'f1_mod_index'); % get data

close all
hist([f1mod.f1_mod_index],30)

%%
ed = [-1.1:0.2:1];
figure(1);clf
subplot(2,1,2)
rsc = fetch(lgnmov_experiment.PairwiseCorrelationsPairs()*lgnmov_experiment.RecordingSession()*...
    lgnmov_experiment.VisualEvokedIndexUnits() & 'recording_site = "v1"' & ...
    'experiment_type = "tf"' & ... % select experiment
    'vevoked > 10','rsc_stationary','rsc_locomotion');
nstims = 5;
rsc_sta = [];
rsc_loc = [];
for iR = 1:length(rsc)
    rsc_sta = [rsc_sta,nanmean(rsc(iR).rsc_stationary(1:5))];
    rsc_loc = [rsc_loc,nanmean(rsc(iR).rsc_locomotion(1:5))];
end
ii = ~isnan(rsc_sta) & ~isnan(rsc_loc);
disp(sum(ii))

plot(rsc_sta,rsc_loc,'ko')
hold all
plot([-1,1],[-1,1])
axis tight
axis square
axis([-1,1,-1,1])

figure(2),clf
subplot(2,1,2)
cnts = histc(rsc_sta,ed);
b = bar(ed,cnts/sum(ii),'histc');
set(b,'facecolor','k','edgecolor','k','facealpha',0.3);
hold all
cnts = histc(rsc_loc,ed);
b = bar(ed,cnts/sum(ii),'histc');
set(b,'facecolor','r','edgecolor','r','facealpha',0.3);
text(-0.5,0.3,num2str(sum(ii)))
figure(1)
subplot(2,1,1)
rsc = fetch(lgnmov_experiment.PairwiseCorrelationsPairs()*lgnmov_experiment.RecordingSession()*...
    lgnmov_experiment.VisualEvokedIndexUnits() & 'recording_site = "lgn"' & ...
    'experiment_type = "tf"' & ... % select experiment
    'vevoked > 10','rsc_stationary','rsc_locomotion');
nstims = 5;
rsc_sta = [];
rsc_loc = [];
for iR = 1:length(rsc)
    rsc_sta = [rsc_sta,nanmean(rsc(iR).rsc_stationary(1:5))];
    rsc_loc = [rsc_loc,nanmean(rsc(iR).rsc_locomotion(1:5))];
end
ii = ~isnan(rsc_sta) & ~isnan(rsc_loc);

plot(rsc_sta,rsc_loc,'ko')
hold all
plot([-1,1],[-1,1])
axis tight
axis square
axis([-1,1,-1,1])

figure(2)
subplot(2,1,1)
cnts = histc(rsc_sta,ed);
b = bar(ed,cnts/sum(ii),'histc');
set(b,'facecolor','k','edgecolor','k','facealpha',0.3);
hold all
cnts = histc(rsc_loc,ed);
b = bar(ed,cnts/sum(ii),'histc');
set(b,'facecolor','r','edgecolor','r','facealpha',0.3);
text(-0.5,0.3,num2str(sum(ii)))
%%

