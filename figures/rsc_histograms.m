%% Histograms of rsc...
sel = lgnmov_experiment.RecordingSession()*lgnmov_experiment.PairwiseCorrelationsPairs() ...
    & 'recording_site = "lgn"' & ... & select brain area
    'experiment_type = "tf"'  % select experiment
lgnpairs = fetch(sel,'frsc_locomotion','frsc_stationary',...
    'zrsc_locomotion','zrsc_stationary',...
    'fzrsc_locomotion','fzrsc_stationary',...
    'bfzrsc_locomotion','bfzrsc_stationary',...
    'rsc_locomotion','rsc_stationary'); % get data

sel = lgnmov_experiment.RecordingSession()*lgnmov_experiment.PairwiseCorrelationsPairs() ...
    & 'recording_site = "v1"' & ... & select brain area
    'experiment_type = "tf"'  % select experiment

v1pairs = fetch(sel,'frsc_locomotion','frsc_stationary',...
    'zrsc_locomotion','zrsc_stationary',...
    'fzrsc_locomotion','fzrsc_stationary',...
    'bfzrsc_locomotion','bfzrsc_stationary',...
    'rsc_locomotion','rsc_stationary'); % get data
close all
%% V1 plots bootstrap zscored full window

nstims = 5;
rsc_loc = zeros(size(v1pairs));
rsc_sta = zeros(size(v1pairs));
for i = 1:length(v1pairs)
    x = v1pairs(i);
    ii = find(~isnan(x.bfzrsc_locomotion(1:nstims)) & ~isnan(x.bfzrsc_stationary(1:nstims)));
    if length(ii) > 1
        rsc_loc(i) = mean(x.bfzrsc_locomotion(ii));
        rsc_sta(i) = mean(x.bfzrsc_stationary(ii));
    else
        rsc_loc(i) = nan;
        rsc_sta(i) = nan;
    end
    
end

idx = ~(isnan(rsc_loc) | isnan(rsc_sta));
exps = unique({v1pairs(idx).experiment});

rsc_loc = rsc_loc(idx);
rsc_sta = rsc_sta(idx);

edges = [-1.1:0.1:1];
figure()
subplot(2,2,[1,3]) % scatters
hold all
plot([-1,1],[-1,1],'k')
scatter(rsc_sta,rsc_loc,15,[0,0,0],'filled','markerfacealpha',0.5)
axis square
title('v1 rsc')
xlabel('rsc stationary')
ylabel('rsc locomotion')
set(gca,'color','none')
text(-0.5,-0.9,[num2str(length(rsc_loc)),' pairs in ',num2str(length(exps)),' sessions'])
subplot(2,2,2) % histograms
hold all
counts = histc(rsc_sta,edges);
countssta = counts/length(rsc_sta);
b = bar(edges,countssta,'histc');
set(b,'edgecolor','k','facecolor','k','facealpha',0.5)
counts = histc(rsc_loc,edges);
countsloc = counts/length(rsc_loc);

m = max(countsloc);
mm = mean(rsc_loc);
plot(mm + [0,0],m+[0.01,0.02],'r')
text(mm + 0.01,m + 0.01,num2str(mm),'color','r')
disp(['Loc mean rsc:',num2str(mm)])
mm = mean(rsc_sta);
plot(mm + [0,0],m+[0.04,0.05],'k')
text(mm + 0.01,m+0.04,num2str(mm),'color','k')
disp(['Sta mean rsc:',num2str(mm)])

b = bar(edges,countsloc,'histc');
set(b,'edgecolor','r','facecolor','r','facealpha',0.5)
set(gca,'color','none')

xlabel('rsc')
ylabel('fraction of pairs')

ylim([0,m+0.1])
xlim([-0.5,0.8])
subplot(2,2,4) % Cumulatives
plot(edges,cumsum(countsloc),'r')
hold all
plot(edges,cumsum(countssta),'k')
set(gca,'color','none')
xlim([-0.5,0.8])
ylim([0,1])
axis square
ylabel('Fraction of pairs')
xlabel('rsc')

print(gcf(),'-dpdf','v1_bootstrapped_fullwindow.pdf')
%% lgn plots bootstrap zscored full window
nstims = 5;
rsc_loc = zeros(size(lgnpairs));
rsc_sta = zeros(size(lgnpairs));
for i = 1:length(lgnpairs)
    x = lgnpairs(i);
    ii = find(~isnan(x.bfzrsc_locomotion(1:nstims)) & ~isnan(x.bfzrsc_stationary(1:nstims)));
    if length(ii) > 1
        rsc_loc(i) = mean(x.bfzrsc_locomotion(ii));
        rsc_sta(i) = mean(x.bfzrsc_stationary(ii));
    else
        rsc_loc(i) = nan;
        rsc_sta(i) = nan;
    end
    
end

idx = ~(isnan(rsc_loc) | isnan(rsc_sta));
exps = unique({lgnpairs(idx).experiment});

rsc_loc = rsc_loc(idx);
rsc_sta = rsc_sta(idx);

edges = [-1.1:0.1:1];
figure()
subplot(2,2,[1,3]) % scatters
hold all
plot([-1,1],[-1,1],'k')
scatter(rsc_sta,rsc_loc,15,[0,0,0],'filled','markerfacealpha',0.5)
axis square
title('lgn rsc')
xlabel('rsc stationary')
ylabel('rsc locomotion')
set(gca,'color','none')
text(-0.5,-0.9,[num2str(length(rsc_loc)),' pairs in ',num2str(length(exps)),' sessions'])
subplot(2,2,2) % histograms
hold all
counts = histc(rsc_sta,edges);
countssta = counts/length(rsc_sta);
b = bar(edges,countssta,'histc');
set(b,'edgecolor','k','facecolor','k','facealpha',0.5)
counts = histc(rsc_loc,edges);
countsloc = counts/length(rsc_loc);

m = max(countsloc);
mm = mean(rsc_loc);
plot(mm + [0,0],m+[0.01,0.02],'r')
text(mm + 0.01,m + 0.01,num2str(mm),'color','r')
disp(['Loc mean rsc:',num2str(mm)])
mm = mean(rsc_sta);
plot(mm + [0,0],m+[0.04,0.05],'k')
text(mm + 0.01,m+0.04,num2str(mm),'color','k')
disp(['Sta mean rsc:',num2str(mm)])

b = bar(edges,countsloc,'histc');
set(b,'edgecolor','r','facecolor','r','facealpha',0.5)
set(gca,'color','none')

xlabel('rsc')
ylabel('fraction of pairs')

ylim([0,m+0.1])
xlim([-0.5,0.8])
subplot(2,2,4) % Cumulatives
plot(edges,cumsum(countsloc),'r')
hold all
plot(edges,cumsum(countssta),'k')
set(gca,'color','none')
xlim([-0.5,0.8])
ylim([0,1])
axis square
ylabel('Fraction of pairs')
xlabel('rsc')
print(gcf(),'-dpdf','lgn_bootstrapped_fullwindow.pdf')


%% V1 0.5,1.5 window
nstims = 5;
rsc_loc = zeros(size(v1pairs));
rsc_sta = zeros(size(v1pairs));
for i = 1:length(v1pairs)
    x = v1pairs(i);
    ii = find(~isnan(x.rsc_locomotion(1:nstims)) & ~isnan(x.rsc_stationary(1:nstims)));
    if length(ii) > 1
        rsc_loc(i) = mean(x.rsc_locomotion(ii));
        rsc_sta(i) = mean(x.rsc_stationary(ii));
    else
        rsc_loc(i) = nan;
        rsc_sta(i) = nan;
    end
    
end

idx = ~(isnan(rsc_loc) | isnan(rsc_sta));
exps = unique({v1pairs(idx).experiment});

rsc_loc = rsc_loc(idx);
rsc_sta = rsc_sta(idx);

edges = [-1.1:0.1:1];
figure()
subplot(2,2,[1,3]) % scatters
hold all
plot([-1,1],[-1,1],'k')
scatter(rsc_sta,rsc_loc,15,[0,0,0],'filled','markerfacealpha',0.5)
axis square
title('v1 rsc')
xlabel('rsc stationary')
ylabel('rsc locomotion')
set(gca,'color','none')
text(-0.5,-0.9,[num2str(length(rsc_loc)),' pairs in ',num2str(length(exps)),' sessions'])
subplot(2,2,2) % histograms
hold all
counts = histc(rsc_sta,edges);
countssta = counts/length(rsc_sta);
b = bar(edges,countssta,'histc');
set(b,'edgecolor','k','facecolor','k','facealpha',0.5)
counts = histc(rsc_loc,edges);
countsloc = counts/length(rsc_loc);

m = max(countsloc);
mm = mean(rsc_loc);
plot(mm + [0,0],m+[0.01,0.02],'r')
text(mm + 0.01,m + 0.01,num2str(mm),'color','r')
disp(['Loc mean rsc:',num2str(mm)])
mm = mean(rsc_sta);
plot(mm + [0,0],m+[0.04,0.05],'k')
text(mm + 0.01,m+0.04,num2str(mm),'color','k')
disp(['Sta mean rsc:',num2str(mm)])

b = bar(edges,countsloc,'histc');
set(b,'edgecolor','r','facecolor','r','facealpha',0.5)
set(gca,'color','none')

xlabel('rsc')
ylabel('fraction of pairs')

ylim([0,m+0.1])
xlim([-0.5,0.8])
subplot(2,2,4) % Cumulatives
plot(edges,cumsum(countsloc),'r')
hold all
plot(edges,cumsum(countssta),'k')
set(gca,'color','none')
xlim([-0.5,0.8])
ylim([0,1])
axis square
ylabel('Fraction of pairs')
xlabel('rsc')

print(gcf(),'-dpdf','v1_1swindow.pdf')
%% lgn plots 0.5,1.5 window
nstims = 5;
rsc_loc = zeros(size(lgnpairs));
rsc_sta = zeros(size(lgnpairs));
for i = 1:length(lgnpairs)
    x = lgnpairs(i);
    ii = find(~isnan(x.rsc_locomotion(1:nstims)) & ~isnan(x.rsc_stationary(1:nstims)));
    if length(ii) > 1
        rsc_loc(i) = mean(x.rsc_locomotion(ii));
        rsc_sta(i) = mean(x.rsc_stationary(ii));
    else
        rsc_loc(i) = nan;
        rsc_sta(i) = nan;
    end
    
end

idx = ~(isnan(rsc_loc) | isnan(rsc_sta));
exps = unique({lgnpairs(idx).experiment});

rsc_loc = rsc_loc(idx);
rsc_sta = rsc_sta(idx);

edges = [-1.1:0.1:1];
figure()
subplot(2,2,[1,3]) % scatters
hold all
plot([-1,1],[-1,1],'k')
scatter(rsc_sta,rsc_loc,15,[0,0,0],'filled','markerfacealpha',0.5)
axis square
title('lgn rsc')
xlabel('rsc stationary')
ylabel('rsc locomotion')
set(gca,'color','none')
text(-0.5,-0.9,[num2str(length(rsc_loc)),' pairs in ',num2str(length(exps)),' sessions'])
subplot(2,2,2) % histograms
hold all
counts = histc(rsc_sta,edges);
countssta = counts/length(rsc_sta);
b = bar(edges,countssta,'histc');
set(b,'edgecolor','k','facecolor','k','facealpha',0.5)
counts = histc(rsc_loc,edges);
countsloc = counts/length(rsc_loc);

m = max(countsloc);
mm = mean(rsc_loc);
plot(mm + [0,0],m+[0.01,0.02],'r')
text(mm + 0.01,m + 0.01,num2str(mm),'color','r')
disp(['Loc mean rsc:',num2str(mm)])
mm = mean(rsc_sta);
plot(mm + [0,0],m+[0.04,0.05],'k')
text(mm + 0.01,m+0.04,num2str(mm),'color','k')
disp(['Sta mean rsc:',num2str(mm)])

b = bar(edges,countsloc,'histc');
set(b,'edgecolor','r','facecolor','r','facealpha',0.5)
set(gca,'color','none')

xlabel('rsc')
ylabel('fraction of pairs')

ylim([0,m+0.1])
xlim([-0.5,0.8])
subplot(2,2,4) % Cumulatives
plot(edges,cumsum(countsloc),'r')
hold all
plot(edges,cumsum(countssta),'k')
set(gca,'color','none')
xlim([-0.5,0.8])
ylim([0,1])
axis square
ylabel('Fraction of pairs')
xlabel('rsc')
print(gcf(),'-dpdf','lgn_1swindow.pdf')