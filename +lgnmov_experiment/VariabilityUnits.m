%{
# Variability and statistics for each unit
-> lgnmov_experiment.RecordingSessionUnits
-> lgnmov_experiment.StimSpikes

---
isi_cv: longblob
isi_cv_locomotion: longblob
isi_cv_stationary: longblob

f0_cv: longblob
f0_cv_locomotion: longblob
f0_cv_stationary: longblob

f1_cv: longblob
f1_cv_locomotion: longblob
f1_cv_stationary: longblob

fano: longblob
fano_locomotion: longblob
fano_stationary: longblob

%}

classdef VariabilityUnits < dj.Part
    properties(SetAccess=protected)
        master = lgnmov_experiment.Variability
    end
    methods
        function makeTuples(self, key)
            stim_window = [0,nan]; %s
            fano_window = 50; %ms
            min_n_trials = 8;
            
            d = fetch((lgnmov_experiment.RecordingSessionUnits() * ...
                lgnmov_experiment.StimSpikesResponses() * ...
                lgnmov_experiment.Stimuli() * ...
                lgnmov_experiment.BehavioralTrialSelection() & key),...
                'f0','f1','trig_spikes','stim_dur','stim_tf','stimtimes','stimofftimes',...
                'locomotion_trials','stationary_trials','discarded_trials');
            
            for iUnit = 1:length(d)
                %%
                d(iUnit).fano = [];
                d(iUnit).fano_locomotion = [];
                d(iUnit).fano_stationary = [];
                
                d(iUnit).isi_cv = [];
                d(iUnit).isi_cv_stationary = [];
                d(iUnit).isi_cv_locomotion = [];
                
                d(iUnit).f0_cv = [];
                d(iUnit).f0_cv_stationary = [];
                d(iUnit).f0_cv_locomotion = [];
                
                d(iUnit).f1_cv = [];
                d(iUnit).f1_cv_stationary = [];
                d(iUnit).f1_cv_locomotion = [];
                
                for iStim = 1:length(d(iUnit).stim_tf)
                    stim_win = stim_window;
                    if isnan(stim_window(end))
                        % use max
                        stim_win(end) = d(iUnit).stim_dur;
                    end
                    % Fano factor all
                    bsp = binary_spiketrains(d(iUnit).trig_spikes{iStim},stim_win,0.001);
                    d(iUnit).fano(iStim) = fano_factor(bsp,fano_window);
                    % isi CV all
                    isis = cellfun(@(x)diff(x),d(iUnit).trig_spikes{iStim},'uniformoutput',0);
                    d(iUnit).isi_cv(iStim) = std(vertcat(isis{:}))/mean(vertcat(isis{:}));
                    % f0 and f1 cv
                    d(iUnit).f0_cv(iStim) = std(d(iUnit).f0(:,iStim))/mean(d(iUnit).f0(:,iStim));
                    d(iUnit).f1_cv(iStim) = std(d(iUnit).f1(:,iStim))/mean(d(iUnit).f1(:,iStim));
                    
                    %discardedidx = d(iUnit).discarded_trials{iStim};
                    % init to nan
                    d(iUnit).fano_locomotion(iStim) = nan;
                    d(iUnit).fano_stationary(iStim) = nan;
                    d(iUnit).isi_cv_locomotion(iStim) = nan;
                    d(iUnit).isi_cv_stationary(iStim) = nan;
                    d(iUnit).f0_cv_locomotion(iStim) = nan;
                    d(iUnit).f0_cv_stationary(iStim) = nan;
                    d(iUnit).f1_cv_locomotion(iStim) = nan;
                    d(iUnit).f1_cv_stationary(iStim) = nan;
                    
                    % locomotion and stationary
                    locidx = d(iUnit).locomotion_trials{iStim};
                    staidx = d(iUnit).stationary_trials{iStim};
                    mintrials = min([length(locidx),length(staidx)]);
                    if mintrials >= min_n_trials
                        %
                        % stationary fano
                        idx = randperm(length(staidx));
                        ts = d(iUnit).trig_spikes{iStim};
                        bsp = binary_spiketrains(ts(staidx(idx)),stim_win,0.001);
                        d(iUnit).fano_stationary(iStim) = fano_factor(bsp,fano_window);
                        % stationary isi 
                        isis = cellfun(@(x)diff(x),ts(staidx(idx)),'uniformoutput',0);
                        d(iUnit).isi_cv_stationary(iStim) = std(vertcat(isis{:}))/mean(vertcat(isis{:}));
                        % stationary f0 and f1 cv
                        d(iUnit).f0_cv_stationary(iStim) = std(d(iUnit).f0(staidx(idx),iStim))/mean(d(iUnit).f0(staidx(idx),iStim));
                        d(iUnit).f1_cv_stationary(iStim) = std(d(iUnit).f1(staidx(idx),iStim))/mean(d(iUnit).f1(staidx(idx),iStim));
                        
                        % locomotion fano
                        idx = randperm(length(locidx));
                        bsp = binary_spiketrains(ts(locidx(idx)),stim_win,0.001);
                        d(iUnit).fano_locomotion(iStim) = fano_factor(bsp,fano_window);
                        % locomotion isi
                        isis = cellfun(@(x)diff(x),ts(locidx(idx)),'uniformoutput',0);
                        d(iUnit).isi_cv_locomotion(iStim) = std(vertcat(isis{:}))/mean(vertcat(isis{:}));
                        % locomotion f0 and f1 cv
                        d(iUnit).f0_cv_locomotion(iStim) = std(d(iUnit).f0(locidx(idx),iStim))/mean(d(iUnit).f0(locidx(idx),iStim));
                        d(iUnit).f1_cv_locomotion(iStim) = std(d(iUnit).f1(locidx(idx),iStim))/mean(d(iUnit).f1(locidx(idx),iStim));
                        
                    end
                end
             
            end
           
            self.insert(d,'REPLACE')
        end
    end
end


function test()
    %% plot a variability plot
    dataset = 'tf';
    recsite = 'lgn'
    dat = fetch(lgnmov_experiment.RecordingSession() *lgnmov_experiment.StimSpikesResponses()* lgnmov_experiment.VisualEvokedIndexUnits() * ...
        lgnmov_experiment.VariabilityUnits() * lgnmov_experiment.LocomotionModulationUnits() & sprintf('recording_site = "%s"',recsite) & 'vevoked >= 10' & sprintf('experiment_type = "%s"',dataset),'isi_cv_locomotion','isi_cv_stationary',...
        'f1','f1_mod_index','fano_locomotion','fano_stationary','f0_cv_locomotion','f0_cv_stationary','f1_cv_locomotion','f1_cv_stationary')
    % Versus the modulation index
    f0_loc = [];
    f0_sta = [];
    f1_loc = [];
    f1_sta = [];
    mod_ind = [];
    for iUnit = 1:length(dat)
        mf1 = nanmean(dat(iUnit).f1);
        idxnan = isnan(dat(iUnit).isi_cv_locomotion);
        mf1(idxnan) = nan;
        %[~,idx] = nanmax(mf1);
        
        f0_loc = [f0_loc,nanmean(dat(iUnit).f0_cv_locomotion)];
        f0_sta = [f0_sta,nanmean(dat(iUnit).f0_cv_stationary)];
        
        f1_loc = [f1_loc,nanmean(dat(iUnit).f1_cv_locomotion)];
        f1_sta = [f1_sta,nanmean(dat(iUnit).f1_cv_stationary)];
        
        mod_ind = [mod_ind,dat(iUnit).f1_mod_index];
    end
    fig = figure(4);clf
    subplot(1,2,1)
    plot([-50,200],[0,0],'color',[0,0,0]+0.5)
    hold all
    plot(mod_ind*100,f0_loc - f0_sta,'ko','markersize',3)
    id = ~isnan(f1_loc);
    P = polyfit(mod_ind(id)*100,f0_loc(id)-f0_sta(id),1);
    
    plot([-50,200],polyval(P,[-100,200]))
    xlabel('f1 modulation index ')
    ylabel('\Delta f0 cv ')
    axis square
    axis([-50,100,-0.5,0.5])
    set(gca,'color','none')
    
    subplot(1,2,2)
    plot([-50,200],[0,0],'color',[0,0,0]+0.5)
    hold all
    plot(mod_ind*100,f1_loc - f1_sta,'ko','markersize',3)
    id = ~isnan(f1_loc);
    P = polyfit(mod_ind(id)*100,f1_loc(id)-f1_sta(id),1);
    
    plot([-50,200],polyval(P,[-100,200]))
    xlabel('f1 modulation index ')
    ylabel('\Delta f1 cv ')
    axis square
    axis([-50,100,-0.5,0.5])
    set(gca,'color','none')
    set(gcf,'papersize',[10,5],'paperposition',[0,0,10,5])
    print(gcf,'-dpdf',sprintf('~/lgnmov_figures/djvariability/f0_f1_CV_modind_variability_%s.pdf',recsite))
% F0 and F1 CV
    f0_loc = [];
    f0_sta = [];
    f1_loc = [];
    f1_sta = [];
    for iUnit = 1:length(dat)
        mf1 = nanmean(dat(iUnit).f1);
        idxnan = isnan(dat(iUnit).isi_cv_locomotion);
        mf1(idxnan) = nan;
%         [~,idx] = nanmax(mf1);
%         
%         f0_loc = [f0_loc,dat(iUnit).f0_cv_locomotion(idx)];
%         f0_sta = [f0_sta,dat(iUnit).f0_cv_stationary(idx)];
%         
%         f1_loc = [f1_loc,dat(iUnit).f1_cv_locomotion(idx)];
%         f1_sta = [f1_sta,dat(iUnit).f1_cv_stationary(idx)];
        f0_loc = [f0_loc,nanmean(dat(iUnit).f0_cv_locomotion)];
        f0_sta = [f0_sta,nanmean(dat(iUnit).f0_cv_stationary)];
        
        f1_loc = [f1_loc,nanmean(dat(iUnit).f1_cv_locomotion)];
        f1_sta = [f1_sta,nanmean(dat(iUnit).f1_cv_stationary)];
    end
    
    fig = figure(1);clf
    subplot(2,2,1)
    hold all,plot([0,2.2],[0,2.2],'k')
    plot(f0_sta,f0_loc,'ko','markersize',3)
    axis([0,0.8,0,0.8])
    axis square
    xlabel('f0 CV stationary')
    ylabel('f0 CV locomotion')
    histedges = [0:0.01:1];
    set(gca,'color','none')
    subplot(2,2,2)
    hold all
    binssta = histc(f0_sta,histedges);
    b = area(histedges,cumsum(binssta)/sum(binssta));
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    
    binsloc = histc(f0_loc,histedges);
    b = plot(histedges,cumsum(binsloc)/sum(binsloc),'r','linewidth',2);
    %set(b,'facecolor','none','edgecolor','r')
    axis([0,1,0,1])
    set(gca,'color','none')
    xlabel('f0 CV')
    ylabel('cell fraction')
   
    %%%%%%%%%%% F1 %%%%%%%%%%%%
    subplot(2,2,3)
    hold all,plot([0,2.2],[0,2.2],'k')
    plot(f1_sta,f1_loc,'ko','markersize',3)
    axis([0,0.8,0,0.8])
    axis square
    xlabel('f1 CV stationary')
    ylabel('f1 CV locomotion')
    histedges = [0:0.001:1];
    set(gca,'color','none')
    subplot(2,2,4)
    hold all
    binssta = histc(f1_sta,histedges);
    b = area(histedges,cumsum(binssta)/sum(binssta));
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    
    binsloc = histc(f1_loc,histedges);
    b = plot(histedges,cumsum(binsloc)/sum(binsloc),'r','linewidth',2);
    %set(b,'facecolor','none','edgecolor','r')
    axis([0,1,0,1])
    set(gca,'color','none')
    xlabel('f1 CV')
    ylabel('cell fraction')
    set(gcf,'papersize',[5,5],'paperposition',[0,0,5,5])
    print(gcf,'-dpdf',sprintf('~/lgnmov_figures/djvariability/f0_f1_CV_variability_%s.pdf',recsite))
    % Ratios if f1 and f0_sta
     figure(3),clf
    
    plot(f1_sta./f0_sta,f1_loc./f0_sta,'ko')
    hold all
    plot([0,8],[0,8],'k')
    axis([0,3,0,3])
    xlabel('f1_{sta}/f0_{sta}')
    ylabel('f1_{loc}/f0_{sta}')
    axis square
    set(gcf,'papersize',[5,5],'paperposition',[0,0,5,5])
    print(gcf,'-dpdf',sprintf('~/lgnmov_figures/djvariability/f1_over_f0sta_%s.pdf',recsite))
    % CV and fano factor
    cvs_loc = [];
    cvs_sta = [];
    fano_loc = [];
    fano_sta = [];
    for iUnit = 1:length(dat)
        mf1 = nanmean(dat(iUnit).f1);
        idxnan = isnan(dat(iUnit).isi_cv_locomotion);
        mf1(idxnan) = nan;
        %[~,idx] = nanmax(mf1);
        
        cvs_loc = [cvs_loc,nanmean(dat(iUnit).isi_cv_locomotion)];
        cvs_sta = [cvs_sta,nanmean(dat(iUnit).isi_cv_stationary)];
        
        fano_loc = [fano_loc,nanmean(dat(iUnit).fano_locomotion)];
        fano_sta = [fano_sta,nanmean(dat(iUnit).fano_stationary)];
    end
    
    fig = figure(2);clf
    subplot(2,2,1)
    hold all,plot([0,2.2],[0,2.2],'k')
    plot(cvs_sta,cvs_loc,'ko','markersize',3)
    axis([0.3,2.2,0.3,2.2])
    axis square
    xlabel('isi CV stationary')
    ylabel('isi CV locomotion')
    histedges = [0:0.05:3];
    set(gca,'color','none')
    subplot(2,2,2)
    hold all
    binssta = histc(cvs_sta,histedges);
    b = area(histedges,cumsum(binssta)/sum(binsloc));
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    
    binsloc = histc(cvs_loc,histedges);
    b = plot(histedges,cumsum(binsloc)/sum(binsloc),'r','linewidth',2);
    %set(b,'facecolor','none','edgecolor','r')
    axis([0.3,2.2,0,1])
    set(gca,'color','none')
    xlabel('isi CV')
    ylabel('cell fraction')
   
    %%%%%%%%%%% FANO %%%%%%%%%%%%
    subplot(2,2,3)
    hold all,plot([0,2.2],[0,2.2],'k')
    plot(fano_sta,fano_loc,'ko','markersize',3)
    axis([0.1,2,0.1,2])
    axis square
    xlabel('fano stationary')
    ylabel('fano locomotion')
    histedges = [0:0.05:3];
    set(gca,'color','none')
    subplot(2,2,4)
    hold all
    binssta = histc(fano_sta,histedges);
    b = area(histedges,cumsum(binssta)/sum(binsloc));
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    
    binsloc = histc(fano_loc,histedges);
    b = plot(histedges,cumsum(binsloc)/sum(binsloc),'r','linewidth',2);
    %set(b,'facecolor','none','edgecolor','r')
    axis([0.1,2.2,0,1])
    set(gca,'color','none')
    xlabel('fano factor')
    ylabel('cell fraction')
    set(gcf,'papersize',[5,5],'paperposition',[0,0,5,5])
    print(gcf,'-dpdf',sprintf('~/lgnmov_figures/djvariability/isi_fano_CV_variability_%s.pdf',recsite))
end