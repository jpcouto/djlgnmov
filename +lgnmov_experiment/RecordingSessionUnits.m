%{
-> lgnmov_experiment.RecordingSession
unit        : smallint   # unit number
---
depth : float
harris_isolation_distance : float
channel : smallint
shank : smallint
spikes : longblob
mean_waveform :longblob
%}

classdef RecordingSessionUnits < dj.Part
    properties(SetAccess=protected)
        master = lgnmov_experiment.RecordingSession
    end
    
end