%{
# Stim spikes
-> lgnmov_experiment.StimSpikes
-> lgnmov_experiment.RecordingSessionUnits
---
trig_spikes: longblob
f0:longblob
f1:longblob
stability : int

%}
classdef StimSpikesResponses < dj.Part
    
    properties(SetAccess=protected)
        master = lgnmov_experiment.StimSpikes
    end
    methods
        function makeTuples(self, key)
            %%
            sigma = 10; % for F1
            binsize = 1; % for F1
            nkey = fetch(lgnmov_experiment.RecordingSessionUnits()*lgnmov_experiment.Stimuli() & key,...
                'spikes','stim_tf','stim_dur','stimtimes','nstims','stimofftimes');
            for iUnit = 1:length(nkey)
                %
                ts_trials = {};
                f0 = [];
                f1 = [];
                for iStim = 1:nkey(iUnit).nstims
                    ts_trials{iStim} = extract_trial_spikes(nkey(iUnit).spikes,...
                        nkey(iUnit).stimtimes{iStim},nkey(iUnit).stimofftimes{iStim});
                    [sf0,sf1]=compute_fourier_visual_responses(ts_trials{iStim},...
                        nkey(iUnit).stim_dur,nkey(iUnit).stim_tf(iStim),sigma,binsize);
                    f0(:,iStim) = abs(sf0);
                    f1(:,iStim) = abs(sf1);
                end 
                %
                nkey(iUnit).trig_spikes = ts_trials;
                nkey(iUnit).f0 = f0;
                nkey(iUnit).f1 = f1;
            end
            self.insert(nkey,'REPLACE')    
        end
    end
end
