%{
# Stim spikes
-> lgnmov_experiment.RecordingSession
-> lgnmov_experiment.Stimuli
%}
classdef StimSpikes < dj.Computed
    methods(Access=protected)
        function makeTuples(self, key)
            
            self.insert(key)
            makeTuples(lgnmov_experiment.StimSpikesResponses, key)
            
        end
    end
end