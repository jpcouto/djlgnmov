%{
# Spike train variability and statistics
-> lgnmov_experiment.RecordingSession
-> lgnmov_experiment.StimSpikes
-> lgnmov_experiment.BehavioralTrialSelection

%}

classdef Variability < dj.Computed
    methods(Access=protected)
        function makeTuples(self, key)
            self.insert(key)
            makeTuples(lgnmov_experiment.VariabilityUnits , key)
        end
    end
end
