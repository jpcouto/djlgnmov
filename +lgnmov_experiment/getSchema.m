function obj = getSchema
persistent schemaObject
if isempty(schemaObject)
    schemaObject = dj.Schema(dj.conn, 'lgnmov_experiment', 'lgnmov_experiment');
end
obj = schemaObject;
end
