%{
# Locomotion modulations and statistics
-> lgnmov_experiment.RecordingSessionUnits
-> lgnmov_experiment.StimSpikes

---
vevoked: float
f0_shuff: longblob
f1_shuff: longblob
%}

classdef VisualEvokedIndexUnits < dj.Part
    properties(SetAccess=protected)
        master = lgnmov_experiment.VisualEvokedIndex
    end
    methods
        function makeTuples(self, key)
            
            sigma = 10; % for F1
            binsize = 1; % for F1
            d = fetch((lgnmov_experiment.RecordingSessionUnits() * ...
                lgnmov_experiment.StimSpikesResponses() * ...
                lgnmov_experiment.Stimuli() & key),...
                'f0','f1','trig_spikes','stim_dur','stim_tf','stimtimes','stimofftimes');
            
            for iUnit = 1:length(d)
                d(iUnit).f0_shuff = [];
                d(iUnit).f1_shuff = [];
                for iStim = 1:length(d(iUnit).stim_tf)
                
                % Shuffled responses
               shuffled_ts_trials = d(iUnit).trig_spikes{iStim};
               for tt = 1:length(shuffled_ts_trials)
                   shuffled_ts_trials{tt} = permute_spike_times(shuffled_ts_trials{tt});%,[0,mode(stim_dur{s})],[]);
               end
               [d(iUnit).f0_shuff(:,iStim),d(iUnit).f1_shuff(:,iStim)]=compute_fourier_visual_responses(shuffled_ts_trials,...
                    d(iUnit).stim_dur,d(iUnit).stim_tf(iStim),sigma,binsize);
                end
                d(iUnit).f1_shuff = abs(d(iUnit).f1_shuff);
                
                mf1 = nanmedian(d(iUnit).f1);
                mf1_s = nanmedian(d(iUnit).f1_shuff);
                stimidx = 1:5;% Uses only the first 5 stimuli...
                
                d(iUnit).vevoked = sum(arrayfun(@(x,y)abs(sqrt(x^2-y^2)),mf1(stimidx),mf1_s(stimidx)));
                
                % Removing if cells have some drift during the experiment
                % if the drift is more than the quarter of all trials
                % neuron is sent no non-visually evoked group
                ntrash1 = [];
                ntrash2 = [];
                for is = 1:length(stimidx)
                    mf0 = d(iUnit).f0(:,is);
                    mf1 = d(iUnit).f1(:,is);
                    th1 = mean(mf0)/2.5;
                    th2 = mean(mf1)/2.5;
                    ntrash1 = [ntrash1, nansum(mf0<th1)];
                    ntrash2 = [ntrash2, nansum(mf1<th2)];
                end
                
                flag1 = nanmedian(ntrash1)>length(mf0)/4;
                flag2 = nanmedian(ntrash2)>length(mf1)/4;
                
                if flag1 && flag2
                    d(iUnit).vevoked = 0;
                    disp('Discarded unit (drift).')
                end
            end
            
            self.insert(d,'REPLACE')
        end
    end
end
