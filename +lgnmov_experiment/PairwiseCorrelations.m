%{
# Pairwise correlations variability and statistics
-> lgnmov_experiment.RecordingSession
-> lgnmov_experiment.StimSpikes
-> lgnmov_experiment.BehavioralTrialSelection

%}

classdef PairwiseCorrelations < dj.Computed
    methods(Access=protected)
        function makeTuples(self, key)
            self.insert(key)
            makeTuples(lgnmov_experiment.PairwiseCorrelationsPairs , key)
        end
    end
end


function test()

%% plot a variability plot
    dataset = 'tf';
    recsite = 'lgn'
    dat = fetch(lgnmov_experiment.RecordingSession() *lgnmov_experiment.PairwiseCorrelationsPairs() & sprintf('recording_site = "%s"',recsite) & sprintf('experiment_type = "%s"',dataset),...
        'rsc','rsc_stationary','rsc_locomotion','rsig','geometric_f1_mod_index')
    %
    
    % Versus the modulation index
    rsc_loc = [];
    rsc_sta = [];
    rsc_loc_blank = [];
    rsc_sta_blank = [];
    g_f1_mod_index = [];
    rsig = [];
    for iPair = 1:length(dat)
        
        rsc_loc_blank = [rsc_loc_blank,dat(iPair).rsc_locomotion(end)];
        rsc_sta_blank = [rsc_sta_blank,dat(iPair).rsc_stationary(end)];
%         rsc_loc = [rsc_loc,dat(iPair).rsc_locomotion(1)];
%         rsc_sta = [rsc_sta,dat(iPair).rsc_stationary(1)];
        rsc_loc = [rsc_loc,nanmean(dat(iPair).rsc_locomotion(1:end-1))];
        rsc_sta = [rsc_sta,nanmean(dat(iPair).rsc_stationary(1:end-1))];
        rsig = [rsig, dat(iPair).rsig];
        g_f1_mod_index = [g_f1_mod_index,dat(iPair).geometric_f1_mod_index];
    end
    fig = figure(1);clf
    subplot(2,2,1)
    plot(rsc_sta,rsc_loc,'ko','markersize',3)
    axis([-1,1,-1,1])
    hold all,plot([-1,1],[-1,1])
    axis square
    set(gca,'color','none')
    subplot(2,2,2)
    hold all
    histedges = [0:0.01:1]
    binssta = histc(abs(rsc_sta),histedges);
    b = area(histedges,cumsum(binssta)/sum(binssta));
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    
    binsloc = histc(abs(rsc_loc),histedges);
    b = plot(histedges,cumsum(binsloc)/sum(binsloc),'r','linewidth',2);
    %set(b,'facecolor','none','edgecolor','r')
    axis([0,1,0,1])
    set(gca,'color','none')
    xlabel('Pairwise correlations')
    ylabel('cell fraction')
    axis square
    % Blank
    subplot(2,2,3)
    plot(rsc_sta_blank,rsc_loc_blank,'ko','markersize',3)
    axis([-1,1,-1,1])
    hold all,plot([-1,1],[-1,1])
    axis square
    set(gca,'color','none')
    title('No visual stim')
    subplot(2,2,4)
    hold all
   
    histedges = [0:0.01:1];
    binssta = histc(abs(rsc_sta_blank),histedges);
    b = area(histedges,cumsum(binssta)/sum(binssta));
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    
    binsloc = histc(abs(rsc_loc_blank),histedges);
    b = plot(histedges,cumsum(binsloc)/sum(binsloc),'r','linewidth',2);
    %set(b,'facecolor','none','edgecolor','r')
    axis([0,1,0,1])
    set(gca,'color','none')
    xlabel('Pairwise correlations')
    ylabel('cell fraction')
    axis square
    set(gcf,'papersize',[10,5],'paperposition',[0,0,10,5])
    
    
    fig = figure(2);clf
    subplot(2,2,1)
    histedges = [-1.05:0.1:1];

    binssta = histc(rsc_sta,histedges);
    b = bar(histedges,binssta,'histc');
    set(b,'facecolor',[0,0,0]+0.5,'edgecolor','k')
    binsloc = histc(rsc_loc,histedges);
    hold all
    b = bar(histedges,binsloc,'histc');
    set(b,'facecolor','none','edgecolor','r')
    axis([-1,1,0,max(binsloc)*1.1])
    set(gca,'color','none')
    set(gcf,'papersize',[10,5],'paperposition',[0,0,10,5])
    
end