%{
# Stimuli trial selection 
-> lgnmov_experiment.Behavior
-> lgnmov_experiment.Stimuli
---
locomotion_trials: longblob
#locomotion_trials_velocity: longblob
stationary_trials: longblob
discarded_trials: longblob
n_total_locomotion_trials: smallint
n_total_stationary_trials: smallint
n_total_discarded_trials: smallint
%}
classdef BehavioralTrialSelection < dj.Imported
    methods(Access=protected)
        function makeTuples(self, key)
            d = fetch((lgnmov_experiment.Behavior() * lgnmov_experiment.Stimuli() & key),...
                'behavior_time','behavior_velocity','stimtimes','stimofftimes');
            %%
            d.locomotion_trials = {};
            d.stationary_trials = {};
            d.discarded_trials = {};
            d.n_total_locomotion_trials = 0;
            d.n_total_stationary_trials = 0;
            d.n_total_discarded_trials = 0;
            %d.locomotion_trials_velocity = {};
            ntime = d.behavior_time;
            nvel = d.behavior_velocity;
            mov_trial_idx = {};
            sta_trial_idx = {};
            for iStim = 1:length(d.stimtimes)
                [mov_trial_idx{iStim},sta_trial_idx{iStim}] = select_locomotion_trials(ntime,nvel,...
                    d.stimtimes{iStim},d.stimofftimes{iStim},1,0.25,0.8);
                d.locomotion_trials{iStim} = find(mov_trial_idx{iStim});
                d.stationary_trials{iStim} = find(sta_trial_idx{iStim});
                d.discarded_trials{iStim} = find(~(mov_trial_idx{iStim}|sta_trial_idx{iStim}));
                
                d.n_total_locomotion_trials = d.n_total_locomotion_trials + length(d.locomotion_trials{iStim});
                d.n_total_stationary_trials = d.n_total_stationary_trials + length(d.stationary_trials{iStim});
                d.n_total_discarded_trials = d.n_total_discarded_trials + length(d.discarded_trials{iStim});
            end
            self.insert(d,'REPLACE')
        end
    end
end


function test()
%% Plot the fraction of trials per experiment
dataset = 'tf';
dat = fetch((lgnmov_experiment.BehavioralTrialSelection()*lgnmov_experiment.RecordingSession() & ...
    sprintf('experiment_type = "%s"',dataset)),'n_total_locomotion_trials','n_total_stationary_trials',...
    'n_total_discarded_trials');

stat = [dat.n_total_locomotion_trials;dat.n_total_stationary_trials;dat.n_total_discarded_trials];
fraction = bsxfun(@rdivide,stat,sum(stat));
mean_fraction = mean(fraction,2);
disp(sprintf('\nThe fraction of trials considered is: %2.2f locomoting; %2.2f stationary and %2.2f discarded',...
    mean_fraction(1),mean_fraction(2),mean_fraction(3)))
end