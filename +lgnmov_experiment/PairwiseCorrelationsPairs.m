%{
# Variability and statistics for each unit
-> lgnmov_experiment.RecordingSession
pair_id: smallint
---
pair: blob

#spcount: longblob              # spike count in window
#fspcount: longblob             # spike count in the trial

rsc: longblob
rsc_stationary: longblob
rsc_locomotion: longblob

zrsc: longblob
zrsc_stationary: longblob
zrsc_locomotion: longblob

frsc: longblob
frsc_stationary: longblob
frsc_locomotion: longblob

fzrsc: longblob
fzrsc_stationary: longblob
fzrsc_locomotion: longblob

bfzrsc: longblob
bfzrsc_stationary: longblob
bfzrsc_locomotion: longblob


rsc_stationary_trimmed: longblob   # same number of trials
rsc_locomotion_trimmed: longblob   # same number of trials

geometric_fr: longblob
geometric_f0_mod_index: float
geometric_f1_mod_index: float
rsig : float
%}

classdef PairwiseCorrelationsPairs < dj.Part
    properties(SetAccess=protected)
        master = lgnmov_experiment.PairwiseCorrelations
    end
    methods
        function makeTuples(self, key)
            stim_window = [0.5,1.5]; %s
            
            min_n_trials = 8;
            vevoked_thresh = 10;
            d = fetch((lgnmov_experiment.RecordingSessionUnits() * ...
                lgnmov_experiment.StimSpikesResponses() * ...
                lgnmov_experiment.Stimuli() * ...
                lgnmov_experiment.VisualEvokedIndexUnits()* ...
                lgnmov_experiment.LocomotionModulationUnits()* ...
                lgnmov_experiment.BehavioralTrialSelection() & key),...
                'f1','trig_spikes','stim_tf','stim_dur','vevoked',...
                'locomotion_trials','stationary_trials','discarded_trials',...
                'f0_mod_index','f1_mod_index');
            fstim_window = [0.,unique(arrayfun(@round,[d.stim_dur]))]; %s
            nd = [];
            pairs = [0,0];
            iPair = 1;
            for iUnit1 = 1:length(d)
                for iUnit2 = iUnit1+1:length(d)
                    if d(iUnit1).vevoked < vevoked_thresh
                        continue
                    end
                    if d(iUnit2).vevoked < vevoked_thresh
                        continue
                    end
                     pair = sort([iUnit1,iUnit2]);
                     rsc_locomotion = [];
                     rsc_stationary = [];
                     
                     rsc = [];
                     zrsc = [];
                     frsc = [];
                     fzrsc = [];
                     bfzrsc = [];
                     
                     zrsc_locomotion = [];
                     frsc_locomotion = [];
                     fzrsc_locomotion = [];
                     bfzrsc_locomotion = [];
                     
                     zrsc_stationary = [];
                     frsc_stationary = [];
                     fzrsc_stationary = [];
                     bfzrsc_stationary = [];
                     
                     rsc_stationary_trimmed = [];
                     rsc_locomotion_trimmed = [];
                     
                     geometric_fr = [];
                     if ~sum(ismember(pair,pairs,'rows'))
                         for iStim = 1:length(d(iUnit1).trig_spikes)
                             rsc(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}, ...
                                 d(iUnit2).trig_spikes{iStim},stim_window,'corr');
                             frsc(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}, ...
                                 d(iUnit2).trig_spikes{iStim},fstim_window,'corr');
                             zrsc(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}, ...
                                 d(iUnit2).trig_spikes{iStim},stim_window,'zscore');
                             fzrsc(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}, ...
                                  d(iUnit2).trig_spikes{iStim},fstim_window,'zscore');
                             bfzrsc(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}, ...
                                  d(iUnit2).trig_spikes{iStim},fstim_window,'zscore',1000);
                             
                             geometric_fr(iStim) = sqrt((nanmedian(cellfun(@length,d(iUnit1).trig_spikes{iStim}))/d(iUnit1).stim_dur ...
                                 - nanmedian(cellfun(@length,d(iUnit2).trig_spikes{iStim}))/d(iUnit2).stim_dur).^2);
                             % locomotion and stationary
                             locidx = d(iUnit1).locomotion_trials{iStim};
                             staidx = d(iUnit1).stationary_trials{iStim};
                             mintrials = min([length(locidx),length(staidx)]);
                             if mintrials >= min_n_trials
                                  rsc_stationary(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(staidx),...
                                      d(iUnit2).trig_spikes{iStim}(staidx),stim_window,'corr');
                                  frsc_stationary(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(staidx),...
                                      d(iUnit2).trig_spikes{iStim}(staidx),fstim_window,'corr');
                                  zrsc_stationary(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(staidx),...
                                      d(iUnit2).trig_spikes{iStim}(staidx),stim_window,'zscore');
                                  fzrsc_stationary(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(staidx),...
                                      d(iUnit2).trig_spikes{iStim}(staidx),fstim_window,'zscore');
                                  bfzrsc_stationary(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(staidx),...
                                      d(iUnit2).trig_spikes{iStim}(staidx),fstim_window,'zscore',1000);
                                  rsc_stationary_trimmed(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(staidx(1:mintrials)),...
                                      d(iUnit2).trig_spikes{iStim}(staidx(1:mintrials)),stim_window,'corr');
                                  
                                  rsc_locomotion(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(locidx),...
                                      d(iUnit2).trig_spikes{iStim}(locidx),stim_window,'corr');
                                  frsc_locomotion(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(locidx),...
                                      d(iUnit2).trig_spikes{iStim}(locidx),fstim_window,'corr');
                                  zrsc_locomotion(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(locidx),...
                                      d(iUnit2).trig_spikes{iStim}(locidx),stim_window,'zscore');
                                  fzrsc_locomotion(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(locidx),...
                                      d(iUnit2).trig_spikes{iStim}(locidx),fstim_window,'zscore');
                                  bfzrsc_locomotion(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(locidx),...
                                      d(iUnit2).trig_spikes{iStim}(locidx),fstim_window,'zscore',1000);
                                  rsc_locomotion_trimmed(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(locidx(1:mintrials)),...
                                      d(iUnit2).trig_spikes{iStim}(locidx(1:mintrials)),stim_window,'corr');
                                  
%                                  idx = randperm(length(staidx));
%                                  rsc_stationary(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(idx(1:mintrials)),...
%                                      d(iUnit2).trig_spikes{iStim}(idx(1:mintrials)),stim_window);
%                                  idx = randperm(length(locidx));
%                                  rsc_locomotion(iStim) = compute_rsc(d(iUnit1).trig_spikes{iStim}(idx(1:mintrials)),...
%                                      d(iUnit2).trig_spikes{iStim}(idx(1:mintrials)),stim_window);
                             else
                                 rsc_locomotion(iStim) = nan;
                                 rsc_stationary(iStim) = nan;
                                 
                                 frsc_locomotion(iStim) = nan;
                                 zrsc_locomotion(iStim) = nan;
                                 fzrsc_locomotion(iStim) = nan;
                                 bfzrsc_locomotion(iStim) = nan;
                                 
                                 frsc_stationary(iStim) = nan;
                                 zrsc_stationary(iStim) = nan;
                                 fzrsc_stationary(iStim) = nan;
                                 bfzrsc_stationary(iStim) = nan;

                                 rsc_locomotion_trimmed(iStim) = nan;
                                 rsc_stationary_trimmed(iStim) = nan;
                                 
                            end
                         end
                         pairs = [pairs;pair];
                         nd(iPair).pair_id = iPair;
                         nd(iPair).pair = pair;
                         nd(iPair).rsc = rsc;
                         nd(iPair).rsc = rsc;
                         nd(iPair).frsc = frsc;
                         nd(iPair).zrsc = zrsc;
                         nd(iPair).fzrsc = fzrsc;
                         nd(iPair).bfzrsc = bfzrsc;     
                              
                         nd(iPair).geometric_fr = geometric_fr;
                         nd(iPair).rsc_stationary = rsc_stationary;
                         nd(iPair).rsc_locomotion = rsc_locomotion;
                         
                         nd(iPair).frsc_stationary = frsc_stationary;
                         nd(iPair).zrsc_stationary = zrsc_stationary;
                         nd(iPair).fzrsc_stationary = fzrsc_stationary;
                         nd(iPair).bfzrsc_stationary = bfzrsc_stationary;
                         
                         nd(iPair).frsc_locomotion = frsc_locomotion;
                         nd(iPair).zrsc_locomotion = zrsc_locomotion;
                         nd(iPair).fzrsc_locomotion = fzrsc_locomotion;
                         nd(iPair).bfzrsc_locomotion = bfzrsc_locomotion;
                         
                         nd(iPair).rsc_stationary_trimmed = rsc_stationary_trimmed;
                         nd(iPair).rsc_locomotion_trimmed = rsc_locomotion_trimmed;
                         
                         nd(iPair).geometric_f0_mod_index = sqrt((d(iUnit1).f0_mod_index - d(iUnit2).f0_mod_index).^2);
                         nd(iPair).geometric_f1_mod_index = sqrt((d(iUnit1).f1_mod_index - d(iUnit2).f1_mod_index).^2);
                         nd(iPair).rsig = corr(nanmean(d(iUnit1).f1(:,1:end-1))',nanmean(d(iUnit2).f1(:,1:end-1))');
                         nd(iPair).experiment = key.experiment;
                         nd(iPair).recording_site = key.recording_site;
                         nd(iPair).experiment_name = key.experiment_name;
                         iPair = iPair+1;
                         
                     end
                end
            end
            if length(nd)
                self.insert(nd,'REPLACE')
            else
                disp('Skipped experiment.')
            end
        end
    end
end
