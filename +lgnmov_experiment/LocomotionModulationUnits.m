%{
# Locomotion modulations and statistics
-> lgnmov_experiment.RecordingSessionUnits
-> lgnmov_experiment.StimSpikes
-> lgnmov_experiment.BehavioralTrialSelection

---
f0_locomotion: longblob  # for each trial
f0_stationary: longblob
f0_discarded: longblob

f1_locomotion: longblob
f1_stationary: longblob
f1_discarded: longblob

f0_mod_index: float
f0_mod_index_r: float
f1_mod_index: float
f1_mod_index_r: float
%}

classdef LocomotionModulationUnits < dj.Part
    properties(SetAccess=protected)
        master = lgnmov_experiment.LocomotionModulation
    end
    methods
        function makeTuples(self, key)
            %%
            d = fetch((lgnmov_experiment.RecordingSessionUnits() * ...
                lgnmov_experiment.StimSpikesResponses() * ...
                lgnmov_experiment.BehavioralTrialSelection() & key),...
                'f0','f1','stationary_trials','locomotion_trials','discarded_trials');
            %%
            for iUnit = 1:length(d)
                nstims = length(d(iUnit).locomotion_trials);
                d(iUnit).f0_stationary = nan([nstims,1]);
                d(iUnit).f0_locomotion = nan([nstims,1]);
                d(iUnit).f0_discarded = nan([nstims,1]);
                d(iUnit).f1_stationary = nan([nstims,1]);
                d(iUnit).f1_locomotion = nan([nstims,1]);
                d(iUnit).f1_discarded = nan([nstims,1]);
                for iStim = 1:nstims
                    %f0
                    d(iUnit).f0_stationary(iStim) = nanmedian(d(iUnit).f0(d(iUnit).stationary_trials{iStim},iStim));
                    d(iUnit).f0_locomotion(iStim) = nanmedian(d(iUnit).f0(d(iUnit).locomotion_trials{iStim},iStim));
                    d(iUnit).f0_discarded(iStim) = nanmedian(d(iUnit).f0(d(iUnit).discarded_trials{iStim},iStim));
                    % f1
                    d(iUnit).f1_stationary(iStim) = nanmedian(d(iUnit).f1(d(iUnit).stationary_trials{iStim},iStim));
                    d(iUnit).f1_locomotion(iStim) = nanmedian(d(iUnit).f1(d(iUnit).locomotion_trials{iStim},iStim));
                    d(iUnit).f1_discarded(iStim) = nanmedian(d(iUnit).f1(d(iUnit).discarded_trials{iStim},iStim));
                end
                %idx = (~isnan(d(iUnit).f0_stationary)) & (~isnan(d(iUnit).f0_locomotion));
                sresp = arrayfun(@(x)d(iUnit).f0(d(iUnit).stationary_trials{x},x),1:nstims,'uniformoutput',0);
                mresp = arrayfun(@(x)d(iUnit).f0(d(iUnit).locomotion_trials{x},x),1:nstims,'uniformoutput',0);
                [d(iUnit).f0_mod_index,d(iUnit).f0_mod_index_r] = modulation_index(sresp,...
                    mresp);
                %idx = (~isnan(d(iUnit).f1_stationary)) & (~isnan(d(iUnit).f1_locomotion));
                sresp = arrayfun(@(x)d(iUnit).f1(d(iUnit).stationary_trials{x},x),1:nstims,'uniformoutput',0);
                mresp = arrayfun(@(x)d(iUnit).f1(d(iUnit).locomotion_trials{x},x),1:nstims,'uniformoutput',0);
                [d(iUnit).f1_mod_index,d(iUnit).f1_mod_index_r] = modulation_index(sresp,...
                    mresp);
                if isnan(d(iUnit).f1_mod_index)
                    d(iUnit).f1_mod_index = 1000;
                    d(iUnit).f0_mod_index = 1000;
                    d(iUnit).f1_mod_index_r = 1000;
                    d(iUnit).f0_mod_index_r = 1000;
                end
            end
            self.insert(d,'REPLACE')
        end
    end
end
