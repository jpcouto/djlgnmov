%{
# Spike
-> lgnmov_experiment.Experiment
experiment_name : varchar(48)
---
experiment_type : varchar(4)
sampling_rate = 0: int
n_channels = 0 : smallint
%}
classdef RecordingSession < dj.Imported
    methods(Access=protected)
        function makeTuples(self, key)
            % Adding multiple keys at the same time because of the way data
            % are stored..
            p = getLGNMOVParameters();
            
            %%
            % Find out if its an imec experiment
            if exist(fullfile(p.maindatafolder,p.scGUIfolder,key.experiment),'dir')
                isimec = true;
                xmlfiles = list_files(fullfile(p.maindatafolder,...
                    p.logsfolder,...
                    key.experiment),'*.log','temp');
                if ~length(xmlfiles)
                    keyboard
                end
                ekey = struct('experiment',key.experiment);
                nkey = fetch(lgnmov_experiment.Experiment() & ekey,'experiment',...
                    'recording_site','probe',...
                    'probe_depth','reference_site');
                
                basefolder = fileparts(xmlfiles{1});
                sortfolder = strrep(basefolder,p.logsfolder,p.scGUIfolder);
                % get the data
                [units, ats, waves, fets, probeinfo, param] = processDataFromscGUI(sortfolder,...
                    {nkey.probe}, [nkey.probe_depth], {nkey.recording_site},...
                    {nkey.reference_site}, 20.0);
                
            else
                isimec = false;
                
                xmlfiles = list_files(fullfile(p.maindatafolder,...
                    p.klustersfolder,...
                    key.experiment),'*.xml','temp');
            end
            for iRun = 1:length(xmlfiles)
                try
                    exptype = p.experiment_types{~cellfun(@isempty,cellfun(@(x)strfind(xmlfiles{iRun},...
                        lower(x)),p.experiment_folder_names,'uniformoutput',0))};
                catch
                    disp(['Skipping... ',xmlfiles{iRun}])
                    continue
                end
                
                ekey = struct('experiment',key.experiment);
                nkey = fetch(lgnmov_experiment.Experiment() & ekey,'experiment',...
                    'recording_site','probe',...
                    'probe_depth','reference_site');
                poffset = fetch(lgnmov_experiment.Experiment() & key,'presentation_offset','encoder_pulses');
                
                if isimec
                    %get the logs to find out what we are talking about
                    clearvars bkey
                    logfiles = list_files(fullfile(p.maindatafolder,p.logsfolder,key.experiment),'*.log','temp');
                    fidx = find(cellfun(@(x)length(strfind(x,exptype)),logfiles));
                    if ~length(fidx)
                        disp(['Skipping experiment',key.experiment])
                        continue
                    end
                    logfile = logfiles{fidx};
                    
                    eventsdatafoldername = strrep(basefolder,p.logsfolder,p.eventsfolder);
                    tmp = list_files(eventsdatafoldername,'*.json');
                    hdr = loadjson(tmp{fidx});
                    %
                    jsoninfodatafoldername = strrep(basefolder,p.logsfolder,p.scGUIfolder);
                    tmp = list_files(jsoninfodatafoldername,'*.json');
                    infofile = loadjson(tmp{1});
                    file_offsets = infofile.data_description.file_offsets;
                    fileidx = find(file_offsets<hdr.stim.event_samples(:,1),1,'last');
                    start_offset = double(file_offsets(fileidx))./double(param.samplingRate);
                    stop_offset = double(file_offsets(fileidx+1))./double(param.samplingRate);
                    ts = {};
                    for ic = 1:length(ats)
                        tmp = double(ats{ic});
                        ts{ic} = tmp(tmp>=start_offset & tmp<stop_offset)-start_offset;
                    end
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%% BEHAVIOR %%%%%%%%%%%%%%%%%
                    
                    [~,runname] = fileparts(xmlfiles{iRun});
                    
                    [stimlog, lograw] = extractFrameTimes(logfile);
                    for s = 1:length(stimlog)
                        stimlog(s).times = stimlog(s).times+poffset.presentation_offset;
                    end
                    lograw.StartTime = lograw.StartTime - poffset.presentation_offset*1e4;
                    %stiminfo = lograw.info;
                    % Get velocity and reward time
                    porttimes = double(lograw.PortInputTimes-lograw.StartTime)*1e-4;
                    portcodes = lograw.PortInputCodes;
                    %%
                    stiminfo = lograw.info;
                    
                    tmp = hdr.enc1.event_samples>=file_offsets(fileidx) & hdr.enc1.event_samples<file_offsets(fileidx+1);
                    enc1 = double(hdr.enc1.event_samples(tmp))./double(param.samplingRate);%porttimes(portcodes==4);
                    tmp = hdr.enc2.event_samples>=file_offsets(fileidx) & hdr.enc2.event_samples<file_offsets(fileidx+1);
                    enc2 = double(hdr.enc2.event_samples(tmp))./double(param.samplingRate);%porttimes(portcodes==5);
                    behavior.reward = porttimes(portcodes==6);
                    
                    
                    %%
                else
                    % Get data for experiments in this session.
                    [units, ts, waves, fets, probeinfo, param] = processDataFromKlusters(xmlfiles{iRun},...
                        {nkey.probe}, [nkey.probe_depth], {nkey.recording_site}, {nkey.reference_site}, 20.0);
                    [~,runname] = fileparts(fileparts(xmlfiles{iRun}));
                    
                    % Process the log file experiment
                    
                    tmp = strsplit(fileparts((xmlfiles{iRun})),'/');
                    logfiles = list_files(fullfile(p.maindatafolder,p.klustersfolder,key.experiment),'*.log','temp');
                    
                    tmp = find(cellfun(@(x)length(strfind(x,runname)),logfiles));
                    if isempty(tmp)
                        error(['No log file for: ',key.experiment])
                    else
                        logfile = logfiles{tmp};
                    end
                    
                    [stimlog, lograw] = extractFrameTimes(logfile);
                    % Fix presentation times
                    %%
                    if (poffset.presentation_offset < 0)
                        rawdatafoldername = fullfile(p.maindatafolder,p.klustersfolder,key.experiment);
                        if ~isempty(strfind(rawdatafoldername,'nlx')) % Need the NEV file to sync stimulus
                            tmp = list_files(rawdatafoldername,'*.nev');
                            if ~isempty(tmp)
                                hdr = ft_read_event(tmp{1});
                                poffset.presentation_offset = double(hdr(2).timestamp-hdr(1).timestamp)/1e6;
                            else
                                disp('NLX file not found.')
                            end
                            
                        end
                        %
                    end
                    for s = 1:length(stimlog)
                        stimlog(s).times = stimlog(s).times+poffset.presentation_offset;
                    end
                    %%
                    lograw.StartTime = lograw.StartTime - poffset.presentation_offset*1e4;
                    
                    stiminfo = lograw.info;
                    % Get velocity and reward time
                    porttimes = double(lograw.PortInputTimes-lograw.StartTime)*1e-4;
                    portcodes = lograw.PortInputCodes;
                    
                    enc1 = porttimes(portcodes==3);
                    enc2 = porttimes(portcodes==4);
                    
                    behavior.reward = porttimes(portcodes==5);
                    
                    start_offset = 0;
                    
                end
                
                revs = 2.0 / (poffset.encoder_pulses); % pulses per rev
                wheel_radius = 5; % cm
                delta = 2 * pi * wheel_radius * revs;
                distance = zeros(size(enc1));
                for ii = 1:length(enc1)-1
                    if mod(length(enc2(enc2 > enc1(ii) & enc2 < enc1(ii+1))),2)
                        distance(ii+1) = +delta;
                    else
                        distance(ii+1) = -delta;
                        
                    end
                end
                if isempty(behavior.reward)
                    behavior.reward=0;
                end
                offsetdist = double(cumsum(distance(find(enc1<behavior.reward(1),1,'last'))));
                if isempty(offsetdist)
                    offsetdist = 0;
                end
                distance = cumsum(distance) - offsetdist;
                %lapsize = 150;
                %plot(enc1(:),mod(distance,lapsize))
                %plotRastergram({behaviour.reward})
                %
                bkey = key;
                
                bkey.laptimes = behavior.reward;
                tmax = max(arrayfun(@(x)max(x.times(:)),stimlog));
                
                ntime = (enc1(1):min(diff(enc1)):enc1(end)) - start_offset;
                dist = interp1(enc1(:) - start_offset,distance(:),ntime);
                [ntime,nvel] = process_velocity(ntime(1:end-1),diff(dist)./diff(ntime),90.,150,tmax);
                
                bkey.behavior_time = ntime;
                bkey.behavior_velocity = nvel;
                
                recsites = {nkey.recording_site};
                unitcount = 0;
                %% Visual Stimuli
                stim_start = arrayfun(@(x)x.times(:,1),stimlog,'uniformoutput',false);
                %if e==14 % something is wrong with sf experiment: 141115_CA038 quick fix!!!
                %
                %    stim_stop = arrayfun(@(x)x.times(:,1)+2,stimlog,'uniformoutput',false);
                %    stim_dur = arrayfun(@(x)x.times(:,1)-x.times(:,1)+2,stimlog,'uniformoutput',false);
                %else
                stim_stop = arrayfun(@(x)x.times(:,2),stimlog,'uniformoutput',false);
                stim_dur = arrayfun(@(x)diff(x.times,[],2),stimlog,'uniformoutput',false);
                
                if isimec
                    nstims = size(stimlog,2);
                    ntrials = max(stimlog(end).trial);
                    %
                    a = parseJSONeventOnsetTimes(hdr.stim,double(param.samplingRate));
                    
                    for istim = 1:nstims
                        stim_start{istim} = a.stimOnsetTimes(:,istim) - start_offset ;
                        stim_stop{istim} = a.stimOffsetTimes(:,istim) - start_offset;
                        stim_dur{istim} = a.stimOffsetTimes(:,istim)-a.stimOnsetTimes(:,istim);
                    end
                end
                %end
                nstims = length(stim_start);
                if strcmp(exptype,'tf')
                    % This is a TF dataset. In cagatay's experiments the temporal
                    % frequencies are 1,2,4,8,16,32,0
                    stim_tf = 2.^[0,1,2,3,4,5];%since 32 hz is not in the paper
                    if nstims == 7
                        stim_tf = [stim_tf,2];
                    end
                    expected_duration = 2; % seconds
                    stim_ori = 90 + zeros(size(stim_tf));
                    stim_sf = 0.02 + zeros(size(stim_tf));
                    % Correct TF
                    %stim_tf = cellfun(@mode,stim_dur).*stim_tf/expected_duration;
                elseif strcmp(exptype,'ori')
                    stim_tf = ones(size(stim_dur))*2.; % Gratings and other stuff has a tf of 2 in Cagatay's dataset
                    expected_duration = 2; % seconds
                    stim_ori = [0,45,90,135,180,225,270,315,0];
                    stim_sf = 0.02 + zeros(size(stim_tf));
                elseif strcmp(exptype,'sf')
                    stim_tf = ones(size(stim_dur))*2.; % Gratings and other stuff has a tf of 2 in Cagatay's dataset
                    expected_duration = 2; % seconds
                    stim_ori = 90 + zeros(size(stim_tf));
                    stim_sf = [0.01, 0.02, 0.04, 0.08, 0.16, 0.32,0];
                    %stim_tf = stim_tf*expected_duration./cellfun(@mode,stim_dur);
                elseif strcmp(exptype,'blank')
                    stim_ori = nan(nstims,1);
                    stim_sf = nan(nstims,1);
                    stim_tf = zeros(nstims,1);
                end
                
                vstimkey = key;
                vstimkey.experiment_name = runname;
                vstimkey.stim_ori = stim_ori;
                vstimkey.stim_tf = stim_tf;
                vstimkey.stim_sf = stim_sf;
                vstimkey.nstims = length(stim_tf);
                vstimkey.ntrials = length(stim_start{1});
                vstimkey.stim_dur = mode(stim_dur{1});
                vstimkey.stimtimes = stim_start;
                vstimkey.stimofftimes = stim_stop;
                
                for iRec = 1:length(recsites)
                    nkey(iRec).sampling_rate = param.samplingRate;
                    nkey(iRec).experiment_type = exptype;
                    nkey(iRec).experiment_name = runname;
                    nkey(iRec).n_channels = probeinfo(iRec).nchanpershank;
                    dj.set('ignore_extra_insert_fields',1)
                    self.insert(nkey(iRec),'REPLACE')
                    lgnmov_experiment.Stimuli().insert(vstimkey,'IGNORE')
                    
                    if exist('bkey','var')
                        bkey.experiment_name = runname;
                        insert(lgnmov_experiment.Behavior(),bkey,'REPLACE')
                    end
                    
                    unitidx = find(strcmp({units.recording_site},recsites{iRec}));
                    nunits = length(unitidx);
                    for iUnit = 1:nunits
                        unitcount = unitcount + 1;
                        unitkey = nkey(iRec);
                        unitkey.depth = units(iUnit).depth;
                        unitkey.unit = unitcount;
                        unitkey.harris_isolation_distance = units(iUnit).HisolationDistance;
                        unitkey.shank = units(iUnit).shank;
                        unitkey.channel = units(iUnit).electrode;
                        if length(waves{1})
                            unitkey.mean_waveform = squeeze(nanmean(waves{iUnit},1))';
                        else
                            unitkey.mean_waveform = nan;
                        end
                        unitkey.spikes = ts{iUnit};
                        lgnmov_experiment.RecordingSessionUnits().insert(unitkey,'IGNORE')
                    end
                end
            end
        end
    end
end
