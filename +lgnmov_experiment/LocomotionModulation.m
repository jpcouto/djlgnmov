%{
# Locomotion modulations and statistics
-> lgnmov_experiment.RecordingSession
-> lgnmov_experiment.StimSpikes
-> lgnmov_experiment.BehavioralTrialSelection

%}

classdef LocomotionModulation < dj.Computed
    methods(Access=protected)
        function makeTuples(self, key)
            self.insert(key)
            makeTuples(lgnmov_experiment.LocomotionModulationUnits, key)
        end
    end
end
