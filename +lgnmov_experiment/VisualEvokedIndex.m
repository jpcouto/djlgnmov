%{
# Index for visually evoked cells
-> lgnmov_experiment.RecordingSession
-> lgnmov_experiment.StimSpikes

%}

classdef VisualEvokedIndex < dj.Computed
    methods(Access=protected)
        function makeTuples(self, key)
            self.insert(key)
            makeTuples(lgnmov_experiment.VisualEvokedIndexUnits , key)
        end
    end
end
